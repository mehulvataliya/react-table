import React, { Fragment } from 'react'
import { Button, Input, Pagination, PaginationItem, PaginationLink } from 'reactstrap'
const LEFT_PAGE = "LEFT";
const RIGHT_PAGE = "RIGHT";
const range = (from, to, step = 1) => {
    let i = from;
    const range = [];
    while (i <= to) {
        range.push(i);
        i += step;
    }
    return range;
}

const PaginationTable = ({ pageSize, pageCount, pageIndex, setPageSize, canPreviousPage, canNextPage, gotoPage, previousPage, nextPage, pageOptions }) => {
//  number button Pagination Table

    const fetchPageNumbers = () => {
        const pageNeighbours = 1;
        const totalNumbers = pageNeighbours * 2 + 3;
        const totalBlocks = Math.min(totalNumbers, pageCount);
        const currentPage = pageIndex + 1;
        const totalPages = pageCount;

        if (totalPages > totalBlocks) {
            const startPage = Math.max(1, currentPage - pageNeighbours);
            const endPage = Math.min(totalPages, currentPage + pageNeighbours);
            let pages = range(startPage, endPage);
            const hasLeftSpill = startPage > 2;
            const hasRightSpill = totalPages - endPage > 1;
            const spillOffset = totalNumbers - (pages.length + 1);
            switch (true) {
                case hasLeftSpill && !hasRightSpill: {
                    const extraPages = range(startPage - spillOffset, startPage - 1);
                    pages = [LEFT_PAGE, ...extraPages, ...pages];
                    break;
                }
                case !hasLeftSpill && hasRightSpill: {
                    const extraPages = range(endPage + 1, endPage + spillOffset);
                    pages = [...pages, ...extraPages, RIGHT_PAGE];
                    break;
                }
                case hasLeftSpill && hasRightSpill:
                default: {
                    pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
                    break;
                }
            }
            return [...pages];
        }
        return range(1, totalPages);
    };
    const pages = fetchPageNumbers();
    return (

        <div>
            <span>
                | Go to page:{' '}
                <Input type='number' defaultValue={pageIndex + 1}
                    onChange={e => {
                        const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                        gotoPage(pageNumber)
                    }}
                    style={{ width: '50px' }} />
            </span>
            <select value={pageSize} onChange={e => {
                setPageSize(Number(e.target.value))
            }}>
                {[5, 10, 20, 30, 50].map(pageSize => (
                    <option key={pageSize} value={pageSize}>
                        Show {pageSize}
                    </option>
                ))}
            </select>
            <span>
                Page{' '}
                <strong>
                    {pageIndex + 1} of {pageOptions.length}
                </strong>{' '}
            </span>
            <Fragment>
                <Pagination
                    className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0">
                    <PaginationItem>
                        <PaginationLink
                            onClick={() => gotoPage(0)}
                            disabled={!canPreviousPage}
                        >
                            {'<<'}
                        </PaginationLink>
                    </PaginationItem>

                    {pages.map((page) => {
                        if (page === LEFT_PAGE)
                            return (
                                <PaginationItem key={page}>
                                    <PaginationLink
                                        onClick={() => previousPage()}
                                    >
                                        {'<'}
                                    </PaginationLink>
                                </PaginationItem>
                            )
                        if (page === RIGHT_PAGE)
                            return (
                                <PaginationItem key={page}>
                                    <PaginationLink
                                        onClick={() => nextPage()}
                                    >
                                        {'>'}
                                    </PaginationLink>
                                </PaginationItem>
                            )
                        return (
                            <PaginationItem key={page}>
                                <PaginationLink onClick={() => gotoPage(page - 1)} className={page-1 === pageIndex ? "active" : ""}>
                                    {page}
                                </PaginationLink>
                            </PaginationItem>
                        )
                    })}
                    <PaginationItem>
                        <PaginationLink
                            onClick={() => gotoPage(pageCount - 1)}
                            disabled={!canNextPage}
                        >
                            {'>>'}
                        </PaginationLink>
                    </PaginationItem>
                </Pagination>
            </Fragment>
            {/* <Button color='primary' onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</Button>{' '}
            {
                    pages.map(page => {
                        if (page === LEFT_PAGE) return <Button key={page} color='primary' onClick={() => previousPage()}>{'<'}</Button>;
                        if (page === RIGHT_PAGE) return <Button key={page} color='primary' onClick={() => nextPage()}>{'>'}</Button>;
                        return <Button key={page} color='success' className='m-1' disabled={page - 1 === pageIndex} onClick={() => gotoPage(page - 1)}>{page}</Button>;
                    }
                    )
                }
            < Button color='primary' className='ms-1' onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>{'>>'}</Button>{' '} */}
        </div >
    )
}

export default PaginationTable