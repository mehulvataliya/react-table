import React, { useMemo, useState } from 'react'
import { useTable, usePagination, useSortBy, useGlobalFilter } from 'react-table'
import { Table } from 'reactstrap'
import { COLUMNS } from './Newcolums'
import { GlobalFilter } from './GlobalFilter'
import './table.css';
import PaginationTable from './PaginationTable'

const INITIAL_PAGE_INDEX = 0;
const INITIAL_PAGE_SIZE = 5;


const ServersideTable = ({ data,fetchData,pagetotal }) => {
    const [sort, setSort] = useState(null);
    const columns = useMemo(() => COLUMNS, []);
    const tableInstance = useTable({
        columns,
        data,
        initialState: { pageIndex: INITIAL_PAGE_INDEX, pageSize: INITIAL_PAGE_SIZE, sortBy: [] },
        manualPagination: true,
        manualSortBy: true,
        disableMultiSort: false,
        manualGlobalFilter: true,
        pageCount: pagetotal,

    }
        , useGlobalFilter
        , useSortBy
        , usePagination)

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        nextPage,
        previousPage,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        setPageSize,
        state,
        setSortBy,
        setGlobalFilter
    } = tableInstance

    const { pageIndex, pageSize, globalFilter } = state

    React.useEffect(() => {
        fetchData && fetchData({ pageIndex, pageSize, globalFilter, sort })
    }, [fetchData, pageIndex, pageSize, globalFilter, sort])

    return (
        <>
            <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
            <div className='container-fluid mt-3'>

            <Table {...getTableProps()} responsive>
                <thead>
                    {
                        headerGroups.map(headerGroup => (

                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {
                                    headerGroup.headers.map(column => (
                                        <th
                                            onClick={() => {
                                                if (!column.disableSortBy) {
                                                    // console.log(column);
                                                    // console.log(column.isSortedDesc);
                                                    const desc =
                                                        column.isSortedDesc === true
                                                            ? undefined
                                                            : column.isSortedDesc === false
                                                                ? true
                                                                : false
                                                    // console.log(desc);
                                                    setSort({
                                                        sortBy: column.id,
                                                        sortDirection: desc ? 'desc' : 'asc'
                                                    });
                                                    setSortBy([{ id: column.id, desc }]);
                                                    // console.log(sort);
                                                }
                                            }
                                            }
                                            {...column.getHeaderProps()}>{column.render('Header')}
                                            <span>
                                                {column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : ''}
                                            </span>
                                        </th>
                                    ))}
                            </tr>
                        ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {
                        page.map(row => {
                            prepareRow(row)
                            return (
                                <tr {...row.getRowProps()}>
                                    {
                                        row.cells.map(cell => {
                                            return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                        })}
                                </tr>
                            )
                        })}

                </tbody>
            </Table>
            {/* simple button pagination */}
            {/* <div>
                <span>
                    | Go to page:{' '}
                    <input type='number' defaultValue={pageIndex + 1}
                        onChange={e => {
                            const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(pageNumber)
                        }}
                        style={{ width: '50px' }} />
                </span>
                <select value={pageSize} onChange={e => {
                    setPageSize(Number(e.target.value))
                }}>
                    {[5, 10, 20, 30, 50].map(pageSize => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </select>
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>{' '}
                <button onClick={() => previousPage()} disabled={!canPreviousPage}>Previous</button>
                <button onClick={() => nextPage()} disabled={!canNextPage}>Next</button>
                <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>{'>>'}</button>{' '}
            </div> */}
            <PaginationTable
                pageIndex={pageIndex}
                pageSize={pageSize}
                pageCount={pageCount}
                setPageSize={setPageSize}
                canPreviousPage={canPreviousPage}
                canNextPage={canNextPage}
                previousPage={previousPage}
                nextPage={nextPage}
                gotoPage={gotoPage}
                pageOptions={pageOptions}
            />

            </div>
        </>
    )
}

export default ServersideTable