import { format } from "date-fns"

export const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        disableFilters: true,
        disableSortBy: true,
    },
    {
        Header: 'first name',
        accessor: 'name',
    },
    {
        Header: 'Email',
        accessor: 'email',
        
    },
    {
        Header: 'User Name',
        accessor: 'username',
        Cell: ({ cell: { value } }) => <span style={{ color: "red" }}>{value}</span>
    },
    // {
    //     Header: 'date of birth',
    //     accessor: 'date_of_birth',
    //     cell: row => format(row.date_of_birth, "MM/dd/yyyy"),
    // },
    {
        Header: 'city',
        accessor: 'address.city',
    },
    {
        Header: 'phone',
        accessor: 'phone',
    }
]
