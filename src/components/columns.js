import { format } from "date-fns"

export const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        disableFilters: true,
    },
    {
        Header: 'first name',
        accessor: 'first_name',
    },
    {
        Header: 'Photo',
        Cell:(props) => {
            return (
                <>
                <div style={{display:'flex',alignItems:'center',width:'150px',justifyContent:'space-between'}}>
                <img src={props.row.original.photo} alt="" />
                <span>{props.row.original.first_name}</span>
                </div>
                </>
                    
            )
        }
    },
    {
        Header: 'last name',
        accessor: 'last_name',
        Cell: ({ cell: { value } }) => <span style={{ color: "red" }}>{value}</span>
    },
    {
        Header: 'date of birth',
        accessor: 'date_of_birth',
        cell: row => format(row.date_of_birth, "MM/dd/yyyy"),
    },
    {
        Header: 'country',
        accessor: 'country',
    },
    {
        Header: 'phone',
        accessor: 'phone',
    }
]
