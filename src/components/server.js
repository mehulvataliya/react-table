import React from 'react'
import ServersideTable from './serversidetable'

const Server = () => {
    const [data, setData] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [pageCount, setPageCount] = React.useState(0)

    const listData = async ({pageIndex,pageSize,globalFilter,sort}) => {
        setLoading(true)
        const response = await fetch(`https://jsonplaceholder.typicode.com/users?_limit=${pageSize}&_page=${pageIndex + 1}&q=${globalFilter || ''}&_sort=${sort?.sortBy || ''}&_order=${sort?.sortDirection || ''}`)
        const data = await response.json()
        const pageCount = Math.ceil(200 / pageSize)
        setPageCount(pageCount)
        // console.log(data)
        setData(data)
        setLoading(false)
    }

    const fetchData = React.useCallback(({ pageSize, pageIndex,globalFilter,sort}) => {
            listData({pageIndex,pageSize,globalFilter,sort})
    }, []);

    return (
        <>
            <ServersideTable fetchData={fetchData} data={data} pagetotal={pageCount}/>
        </>
    )

}

export default Server