import React from 'react'
import { Input } from 'reactstrap'

export const GlobalFilter = ({ filter, setFilter }) => {
    return (
        <span className='m-3 d-flex align-items-center'>
            Search:{' '}<Input className=' m-3 w-25' value={filter || ''}
                onChange={e => setFilter(e.target.value)}
                placeholder="Search..." />
        </span>
    )
}
